import org.jnetpcap.Pcap;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;

/**
 * Класс проводит анализ пакетов адаптера
 *
 * @author Максим Лебедев
 */
public class PcapLoopThread extends Thread implements InterfaceElements {

    Pcap pcap;
    boolean pcapFlag;

    /**
     * Сеттер для флага, который отвечает за остановку анализа
     * @param pcapFlag флаг
     */
    public void setPcapFlag(boolean pcapFlag) {
        this.pcapFlag = pcapFlag;
    }

    /**
     * Сеттер для объекта типа Pcap, с помощью которого производится анализ
     * @param pcap объект типа Pcap
     */
    public void setPcap(Pcap pcap) {
        this.pcap = pcap;
    }

    PcapPacketHandler jpacketHandler = new PcapPacketHandler() {

        // строка данных
        String readData;
        byte[] bufferData = new byte[2000];

        @Override
        public void nextPacket(PcapPacket packet, Object o) {
            // данные фрейма data
            byte[] data = packet.getByteArray(0, packet.size());
            input.setText("");
            readData = "";
            // количество байт фрейма
            label2.setText(String.format("Lenght of packet %d bytes", data.length));
            label3.setText(String.format("Frame number %d ", packet.getFrameNumber()));
            // перенос данных фрейма в форматированную строку
            for (int i = 0; i < data.length; i++) {
                bufferData[i] = data[i];
                readData = readData + String.format("%02X ", bufferData[i]);
            }
            // данные фрейма в окно input
            input.setText(readData);
        }
    };

    /**
     * Переопределение метода run
     */
    @Override
    public void run() {
        while (true) {
            try {
                sleep(200);
                if (pcapFlag) {
                    // отлов одного пакета если был Start
                    pcap.loop(1, jpacketHandler, "jnetpcap rocks!");
                }
            } catch (InterruptedException e) {
            }
        }
    }
}