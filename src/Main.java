import javax.swing.*;

/**
 * Главный класс проекта, запускает окно с элементами
 *
 * @author Максим Лебедев
 */
public class Main {

    public static void main(String[] args) {
        // создание объектов
        Form form = new Form();
        // по зарытию формы
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Запуск формы
        form.setVisible(true);
    }
}