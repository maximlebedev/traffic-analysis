import javax.swing.*;

/**
 * Интерфейс содержит все элементы окна программы
 *
 * @author Максим Лебедев
 */
public interface InterfaceElements {

    JButton stop = new JButton("Stop");
    JButton start = new JButton("Start");
    JTextArea input = new JTextArea("input");
    JScrollPane scrollPaneInput = new JScrollPane(input);
    JLabel label1 = new JLabel("Select an adapter");
    JLabel label2 = new JLabel("Test1");
    JLabel label3 = new JLabel("Test2");
    JComboBox adapterList = new JComboBox();
}

