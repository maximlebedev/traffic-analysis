import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Класс элементов интерфейса
 *
 * @author Максим Лебедев
 */
class Form extends JFrame implements InterfaceElements {

    int adapter;
    Pcap pcap = new Pcap();
    List allDevices = new ArrayList();
    StringBuilder errMsg = new StringBuilder();
    int devicesStatus = Pcap.findAllDevs(allDevices, errMsg);
    // массив строк адаптеров
    String [] nbAdapterStrings = {"0", "1"};
    String [] nbAdpStrings = {"0", "1"};
    int snaplen = 64 * 1024;
    int flags = Pcap.MODE_PROMISCUOUS;
    int timeout = 100;

    /**
     * Конструктор класса Form, находит сетевые адаптеры и заполняет ими ComboBox
     */
    public Form() {

        System.out.println("Devices status: " + devicesStatus);

        if (devicesStatus != Pcap.OK) {

            JOptionPane.showMessageDialog(Form.this,
                    new String[]{"Can't read list of devices, error is %s" + errMsg
                            .toString()},
                    "Error read list of devices",
                    JOptionPane.ERROR_MESSAGE);

            return;
        }

        String deviceList = "";
        int i = 0;

        // список адаптеров в input
        for (Iterator iterator = allDevices.iterator(); iterator.hasNext(); ) {

            PcapIf device = (PcapIf) iterator.next();
            String description =
                    (device.getDescription() != null) ? device.getDescription()
                            : "No description available";
            // записать название адаптера в строку
            nbAdapterStrings[i] = description + "\n";
            nbAdpStrings[i] = description;
            deviceList = deviceList + nbAdapterStrings[i];
            // список адаптеров в adapterList
            adapterList.addItem(nbAdapterStrings[i]);
            i++;
        }

        System.out.println("Network devices found: " + deviceList);

        input.setText(deviceList);

        initComponents();
    }

    /**
     * Метод инициализации компонентов формы
     */
    private void initComponents() {
        setTitle("Анализатор трафика");
        // положение на экране
        setBounds(15, 30, 800, 600);
        // размер формы
        setSize(830, 600);
        // Закрытие формы
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Контейнер для размещения компонентов формы
        Container container = getContentPane();
        // установить разметку
        container.setLayout(null);
        container.setBounds(5, 5, 800, 600);

//----------------------------------------------------------------------
// JTextArea

        // Добавление JTextArea input
        input.setLineWrap(true);
        input.setColumns(20);
        input.setRows(5);
        input.setBounds(10, 220, 790, 300);
        container.add(input);
        input.setEditable(false);
        input.setText("");
        // Добавление скрола
        scrollPaneInput.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPaneInput.setBounds(10, 220, 800, 300);
        container.add(scrollPaneInput);
        scrollPaneInput.setViewportView(input);

//----------------------------------------------------------------------
// JComboBox

        adapterList.setBounds(10, 25, 300, 20);
        // зарегистрировать экземпляр класса обработчика события
        adapterList.addActionListener(new ethListEventListener());
        adapterList.setEnabled(true);
        container.add(adapterList);

//----------------------------------------------------------------------
// JLabel

        // добавление метки состояния COM порта
        label1.setBounds(10, 5, 300, 20);
        // прозрачный фон
        label1.setOpaque(true);
        label1.setForeground(Color.red);
        container.add(label1);
        //
        label2.setBounds(10, 190, 300, 20);
        label2.setText("Lenght of packet");
        container.add(label2);
        //
        label3.setBounds(20, 530, 250, 20);
        label3.setText("Frame number");
        container.add(label3);

//----------------------------------------------------------------------
// JButton

        // зарегистрировать экземпляр класса обработчика события start
        start.addActionListener(new startEventListener());
        // добавить кнопку и ее положение
        start.setBounds(440, 155, 80, 25);
        start.setEnabled(false);
        container.add(start);

        // зарегистрировать экземпляр класса обработчика события stop
        stop.addActionListener(new stopEventListener());
        stop.setBounds(730, 155, 80, 25);
        stop.setEnabled(false);
        container.add(stop);
    }

    PcapLoopThread pcapLoopThread;

    /**
     * Класс имплементации события нажатия start
     */
    class startEventListener implements ActionListener {

        @Override
        // обработка события нажатия на button start
        public void actionPerformed(ActionEvent e) {
            // дезактивировать выбор адаптеров
            adapterList.setEnabled(false);


            pcapLoopThread = new PcapLoopThread();
            pcapLoopThread.setPcapFlag(true);
            pcapLoopThread.setPcap(pcap);
            pcapLoopThread.start();

            stop.setEnabled(true);
        }
    }

    /**
     * Класс имплементации события нажатия stop
     */
    class stopEventListener implements ActionListener {
        @Override
        // обработка события нажатия на button stop
        public void actionPerformed(ActionEvent e) {
            // Флаг закрытия pcap
            System.out.println(nbAdpStrings[adapter] + " - close");
            label1.setOpaque(true);
            label1.setForeground(Color.red);
            label1.setText("Select an adapter");

            pcapLoopThread.setPcapFlag(false);
            pcapLoopThread.pcap.close();

            start.setEnabled(false);
            adapterList.setEnabled(true);
            stop.setEnabled(false);
        }
    }

    /**
     * Обработка события изменения JComboBox comList
     */
    class ethListEventListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            // comName - выбранная строка в JComboBox comList
            JComboBox cb = (JComboBox) e.getSource();
            // получить номер выбранного адаптера
            adapter = cb.getSelectedIndex();
            // выбор адаптера
            PcapIf device = (PcapIf) allDevices.get(adapter);
            // отрыть выбранный адаптер

            pcap = Pcap.openLive(device.getName(), snaplen, flags, timeout, errMsg);
            if (pcap == null) {
                JOptionPane.showMessageDialog(Form.this,
                        new String[]{"Error while opening device for capture: " + errMsg
                                .toString()},
                        "Error read list of devices",
                        JOptionPane.ERROR_MESSAGE);

                input.setText("Error while opening device for capture: "
                        + errMsg.toString());
            }
            // выбранный адаптер в окне input
            input.setText(nbAdpStrings[adapter] + " - opened");
            // прозрачность label1 с послед. уст. цвета
            label1.setOpaque(true);
            label1.setForeground(Color.MAGENTA);
            label1.setText(nbAdapterStrings[adapter]);
            start.setEnabled(true);
        }
    }
}