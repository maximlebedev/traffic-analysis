import org.jnetpcap.PcapClosedException;
import org.junit.Test;

import javax.swing.*;

public class EthernetSnifferTest {

    @Test(expected = IllegalThreadStateException.class)
    public void MultipleLaunchTest() {
        PcapLoopThread pcapLoopThread = new PcapLoopThread();
        pcapLoopThread.start();
        pcapLoopThread.start();
    }

    @Test(expected = PcapClosedException.class)
    public void NoInetTest() {
        PcapLoopThread pcapLoopThread = new PcapLoopThread();
        pcapLoopThread.start();
    }

    @Test(expected = UnsatisfiedLinkError.class)
    public void NoLibraryInOSTest() {
        Form form = new Form();
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.setVisible(true);
    }
}